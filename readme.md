# Rust SDL2 Canvas Webassembly

A minimal example of animating an HTML5 canvas from Rust using SDL2 through WebAssembly.

[Live demo]

(If you want more sophisticated drawing than SDL can offer (e.g. 3D), then you should probably start [here] instead.)

[Live demo]: https://thibaultlemaire.gitlab.io/rust-sdl-canvas-wasm/
[here]: https://github.com/timhutton/opengl-canvas-wasm

## Recommended Instructions

1. Install [Nix]. Nix is a functional package manager that simplifies the process of setting up a project environment.

1. Clone this repo:

    ``` sh
    git clone https://gitlab.com/ThibaultLemaire/rust-sdl-canvas-wasm

    cd rust-sdl-canvas-wasm
    ```

1. Start a Nix shell. This will give you an environment with the required dependencies.

    ``` sh
    nix-shell
    ```

1. Install the appropriate toolchain.

    ``` sh
    rustup override set 1.39.0
    rustup target add wasm32-unknown-emscripten
    ```

1. Install `cargo-web`. This cargo sub-command makes it easy to build wasm projects with Rust.

    ``` sh
    cargo install cargo-web
    ```

1. Build and run the project.

    ``` sh
    cargo web start --use-system-emscripten
    ```

1. Open the URL that was output by this last command (Usually [localhost on port 8000]).
    You should see a moving blue square in a red square:

    ![simplescreenrecorder-2020-01-18_22.22.23](https://gitlab.com/ThibaultLemaire/rust-sdl-canvas-wasm/-/wikis/uploads/e2fc00f20eec70ee31e021accde67c3f/simplescreenrecorder-2020-01-18_22.22.23.webm)

[Nix]: https://nixos.org/nix/
[localhost on port 8000]: http://localhost:8000

## Credits

This is a port of [Tim Hutton]'s C++ example, [sdl-canvas-wasm], to the Rust programming language.
In my opinion, his work has the perfect balance of simplicity vs demonstrativity.
I thought the Rust community could use an equivalent.

I also took some inspiration from the work of [Valerio Santinelli] and [bokuweb] for the use of `cargo-web` and the emscripten boilerplate code.

Here are their respective projects:

- [rust-sdl2-wasm]
- [rustynes]

[Tim Hutton]: https://github.com/timhutton
[sdl-canvas-wasm]: https://github.com/timhutton/sdl-canvas-wasm
[Valerio Santinelli]: https://github.com/tanis2000
[bokuweb]: https://github.com/bokuweb
[rust-sdl2-wasm]: https://github.com/tanis2000/rust-sdl2-wasm
[rustynes]: https://github.com/bokuweb/rustynes

## MIT license

``` txt
Copyright (c) 2018 Tim Hutton
Copyright (c) 2019-2020 Thibault Lemaire

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
