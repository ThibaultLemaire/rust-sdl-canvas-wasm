#! /usr/bin/env nix-shell
#! nix-shell -i sh

rustup override set 1.39.0
rustup target add wasm32-unknown-emscripten
cargo install cargo-web
cargo web deploy --release --use-system-emscripten --output ./public
